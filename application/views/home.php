<!DOCTYPE html>
<html lang="en">
    
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Taffy and Tim long awaited day</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link href='http://fonts.googleapis.com/css?family=Kelly+Slab' rel='stylesheet'
        type='text/css'>
        <link href="<?php echo base_URL();?>css/reset.css" rel="stylesheet">
        <link href="<?php echo base_URL();?>css/parallax_style_1.1.3.css" rel="stylesheet" />
        <link href="<?php echo base_URL();?>css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_URL();?>css/style.css" rel="stylesheet" />
        <link href="<?php echo base_URL();?>css/about_slider/lean-slider.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_URL();?>css/camera.css" />
        <link rel="stylesheet" href="<?php echo base_URL();?>css/blur.css" />
        <!--blur slidShow -->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_URL();?>icon/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_URL();?>icon/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_URL();?>icon/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_URL();?>icon/apple-touch-icon-57-precomposed.png">
        <link rel="shortcut icon" href="<?php echo base_URL();?>icon/favicon.ico">
    </head>
    
    <body class="clearfix" data-spy="scroll" data-target="#navbar" data-offset="10">
        <div id="navbar" class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
 <span class="icon-bar"></span>
 <span class="icon-bar"></span>

                    </a>
                    <a class="brand hidden-phone" href="<?php echo base_URL();?>#">Taffy &#38; Tim's Wedding
                    </a>
                    <div class="nav-collapse collapse pull-right">
                        <ul class="nav">
                            <li>
                                <a href="#About">About Us</a>
                            </li>
                            <li>
                                <a href="#intro">Our Proposal</a>
                            </li>
                            <li>
                                <a href="#wedding_party">Wedding party</a>
                            </li>
                            <li>
                                <a href="#rsvp">RSVP</a>
                            </li>
                            <li>
                                <a href="#details">Wedding Details</a>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid clearfix">
            <div class="row-fluid">
                <div id="welcome">
                    <div class="container-fluid clearfix" style="padding:0; overflow:hidden">
                        <ul class="cb-slideshow">
                             <li><span>Image 01</span>
                                <div>
                                    
                                </div>
                            </li>-->
                            <li><span>Image 02</span>
                                <div>
                                   
                                </div>
                            </li>
                            <li><span>Image 03</span>
                                <div>
                                   
                                </div>
                            </li>
							<li><span>Image 04</span>
                                <div>
                                  
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div id="About">
                    <div class="container-fluid clearfix About">
                        <div class="container clearfix">
                            <div class="container clearfix TitleSection">
                                
						<h1>About  US </h1> 

                            </div>
                            <div class="container clearfix">
                                <div class="span12">
                                    <div class="span6">
                                        <div class="clearfix">
                                            <div class="round-thumb-container">
                                                <div class="round-thumb"> <span><img src="images/circlegallery/1.jpg"></span>

                                                </div>
                                            </div>
											<h2>My first impression of Terrell</h2>
											 <br />

                                            <p>When I first met Terrell I was totally nervous like a high school girl. I thought to myself, "Whoa he's tall, and very good-looking!" His calming and gentle demeanor quickly took my nervous feelings away and I felt right at ease as we drove away from the airport. Fast forward to a year and a half after that, moving across the country for love has been the biggest leap I have ever taken in my life. Thankfully, it is the best decision I have ever made. Terrell has pushed and motivated me more than anyone has to shoot for my dreams no matter what the circumstances are, and I am thankful for that. There were times when I doubted myself and he has been right by my side to cheer me on and give me that extra push I needed. I look forward to our journey ahead in which we will have the opportunity to love, grow, succeed, and accomplish everything we have in store for us. I love you!</p>
         
                                        </div>
                                    </div>
                                    <div class="span6">
                                        <div class="clearfix">
                                            <div class="round-thumb-container">
                                                <div class="round-thumb"> <span><img src="images/circlegallery/2.jpg"></span>

                                                </div>
                                            </div>
                                             <h2>My first impression of Taffy</h2>
											 <br />
                                            <p>My first impression of Taffy was a question to myself&#8212; she's cute, but why is her jeans ridiculously ripped showing zebra prints?!?... lol... I was little nervous, but she quickly eased my nerves&#8212; when one of the first things she said to me, "YOUR'RE TALL", then patted me on the head. My inside reaction was&#8212; did she really just pat me on the head? From there, our relationship took off. Her loving affection, caring heart, and independent swagger are some of the qualities I love about her.  She has given up a lot to move to Boston to be with me and I cannot express in words what that has meant.  She has this drive to always do better and even checking me along the way to make sure I'm not slipping. The compassion she shows others and willing to listen and sympathize makes her a great person to be around and friend. God willing and till the end, I will love her with all my heart, today, tomorrow and for all eternity.</p>
 
                                        </div>
                                    </div>
							</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--#About-->
                <div id="intro" class="Parallax">
                    <div class="ParallaxText">
                        	<h2>Our proposal story</h2>

                        <div class="clearfix"></div>
                        <blockquote class="center-no">Where do I begin? First off, I have NEVER been surprised to this degree in my life. We flew to California for Mima and Aaron's wedding which was the day before the proposal. May I add that I caught the bouquet! The day after their wedding (our proposal day) Terrell was acting a bit different than usual, (wanting to go shopping, wanting to eat brunch) these are things that he wouldn't normally suggest we do especially the last day of vacation. He was clearly waiting for time to pass while (unknown to me) my family was preparing for family and friends to arrive at my Mom's house in just a couple hours.</blockquote>
						
						 <blockquote class="center-no">When we arrived to my Mom's house, I noticed a lot of cars outside. I was told we were having a "small" get together, but I knew something was up when everyone was there before us. Anyone who knows my family knows that they usually arrive on "Hawaiian time", which means 1-2 hours after the time you tell them! As we walked into the backyard, I saw my Grandma, Aunts, Uncles, cousins, my sisters, friends from high school, and even Terrell's family who flew in from Boston. I was immediately overwhelmed and got emotional very quick. After a couple minutes of greeting everyone, Terrell grabbed my hand and as we stood before everyone he started off by saying, "As everyone knows, Taffy moved to Boston over 5 years ago to be with me..." That's all I really remember and before I knew it he was on one knee with a ring in his hand! </blockquote>
                        <div class="clearfix"></div>
						
                    </div>
                </div>
                <!--#intro-->
                <div id="Services">
                    <div class="container-fluid clearfix Services">
                        <div class="container clearfix TitleSection">
                            
<h1>Photo Gallery </h1> 

                        </div>
                        <div class="container clearfix">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="slider-wrapper">
                                        <div id="slider">
                                            <div class="slide1">
                                                <img src="images/1.jpg" alt="" />
                                            </div>
                                            <div class="slide2">
                                                <img src="images/2.jpg" alt="" />
                                            </div>
                                            <div class="slide3">
                                                <img src="images/3.jpg" alt="" />
                                            </div>
                                            <div class="slide4">
                                                <img src="images/4.jpg" alt="" />
                                            </div>
                                            <div class="slide5">
                                                <img src="images/5.jpg" alt="" />
                                            </div>
                                            <div class="slide6">
                                                <img src="images/6.jpg" alt="" />
                                            </div>
                                            <div class="slide7">
                                                <img src="images/7.jpg" alt="" />
                                            </div>
                                        </div>
                                        <div id="slider-direction-nav"></div>
                                        <div id="slider-control-nav"></div>
                                    </div>
                                </div>
                            </div>
						</div>
                        </div>
                    </div>
                </div>

                <!--#second-->
                <div id="wedding_party">
                    <div class="container-fluid clearfix Portfolio">
                        <div class="container clearfix TitleSection">
                            
						<h1>Wedding Party</h1> 

                        </div>
                        <div class="container clearfix">
                            <div class="span12">
                            </div>
                            <div class="row-fluid">
                                <div class="span12">
                                    <nav id="filter"></nav>
                                    <section id="container">
                                        <ul id="stage">
                                            <li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <a href="#tiffanyModal" role="button" data-toggle="modal"><span><img src="images/portfolio/tiffany.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Maid Of Honor</h4> 
													<p><a href="#tiffanyModal" role="button" data-toggle="modal">Tiffany Basque</a></p>
														<!-- Modal -->
														<div id="tiffanyModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Tiffany Basque</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff1.jpg" />
															  </p>
															  
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff2.jpg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff3.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff4.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff5.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff6.jpg" />
															  </p>	
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff7.jpg" />
															  </p>	
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff8.jpg" />
															  </p>	
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff10.jpg" />
															  </p>	
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff11.jpg" />
															  </p>	
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff12.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff13.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff14.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff15.png" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff16.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/tiff17.jpg" />
															  </p>

															  </div>
															  <div class="span6"><p>This couple right here... is the next power couple of the century. Taffy has been my rib since the day she was brought into this world.  I have seen her grow into the amazing woman she is today.  There is nothing she can't do when she sets her mind to it. She really met her match with Terrell or "T Money" as I call him. He is intelligent and also very unstoppable!   Together I know they will do great things. I am so incredibly happy for my lil sis and big bro and wish them endless years of happiness, joy and most importantly....LOVE! </p></div>
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> 
													<a href="#johnetteModal" role="button" data-toggle="modal">
													<span><img src="images/portfolio/johnette.jpg" alt="Portfolio Filter" /></span>
													</a>
                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Bridesmaid </h4> 
													<p><a href="#johnetteModal" role="button" data-toggle="modal">Johnette Basque</a></p>
														<!-- Modal -->
														<div id="johnetteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Johnette Basque</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/johnette1.jpeg" />
															  </p>
															  
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/johnette2.jpeg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/johnette3.jpeg" />
															  </p>
															  
															  </div>
															  <div class="span6"><p>I remember when my sister first told me about T...her face lite up with happiness. News to me was that they have been talking for years and she had been back and fourth to Boston to see this dude! I knew it was serious! Meeting T was interesting lol. I knew I loved his ancient. Getting to know T is still ongoing considering they live in Boston but the man I know of is a great guy especially for my sister! I think he balances her and pushes her to achieve anything she wants and desires. I can't wait to see their love flourish and bloom. I know that she is amazingly happy. I CAN'T BELIEVE MY BABY SISTER IS GETTING MARRIED!!! I wish you both the best! Love you guys!</p></div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> 
													<a href="#andreaModal" role="button" data-toggle="modal">
													<span><img src="images/portfolio/andrea.jpg" alt="Portfolio Filter" /></span>
													</a>
                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Bridesmaid </h4> 
													 <p><a href="#andreaModal" role="button" data-toggle="modal">Andrea Gandy</a></p>
														<!-- Modal -->
														<div id="andreaModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Andrea Gandy</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/andrea1.jpeg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/andrea2.jpeg" />
															  </p>
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/andrea3.jpeg" />
															  </p>	 
															  </div>
															  <div class="span6"><p>10 years ago I was blessed to meet one of the most amazing women I know... my BFF.  It was a Friday night birthday dinner for a mutual friend at Friday's Where Taffy and I instantly connected and my life was changed because a lifelong friendship was born...Through the years we've shared so many laughs cries fears and dreams... You've been my ride or die my therapist, my date, my rock and I am so fortunate to call you my best friend... When I reflect over my life it will be forever enriched because of you...  The epitome of beauty... I love you Taffy and congratulations! </p></div>
														  </div>
														</div>
                                                </div>
                                            </li>

											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    
													<div class="filter-thumb">
													<a href="#celinaModal" role="button" data-toggle="modal">
														<span><img src="images/portfolio/celina.jpg" alt="Portfolio Filter" /></span>
													</a>
                                                    </div>
													
                                                </div>
                                                <div>
                                                     <h4>Bridesmaid </h4> 
													<p><a href="#celinaModal" role="button" data-toggle="modal">Celina Ancheta</a></p>
														<!-- Modal -->
														<div id="celinaModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Celina Ancheta</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/celina1.jpeg" />
															  </p>
															  
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/celina2.jpeg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/celina3.jpeg" />
															  </p>
															  
															  </div>
																	  <div class="span6"><p>I have known Taffy for about 11 years now. She has been one of my true, loyal friends ever since we met. Taffy and I met our freshman year, fall semester in 2003 in Sacramento through mutual friends which includes our other partner in crime Andrea at a house party and we exchanged numbers and have been the best of friends since. We partied all throughout our early twenties, went on lots of girl vacations (Vegas, Miami, San Diego, LA, etc.), sporadic trips (like let's just get in the car and go to San Francisco or Santa Cruz or since you're stuck in Vegas, I'll drive and meet you there), and had lots of staybin movie night sleep overs. We have had many memories and of course more to come in the present and future. Even though Taffy moved to Boston and I have moved to Phoenix we still are best friends and try to talk or face time once a week and we still go on trips together whether it's meeting in a city or I fly to Boston or she flies to Phoenix. We make sure no matter what we will still be close and will never let the long distance break us apart. We will always be there for each other through thick and thin. I thank God every day for blessing me with Taffy as my best friend, my family. I am so happy that she met Terrell, the love of her life and thankful to be a part of their coming Wedding Day. I love you both!!! Congratulations!!! Love always and forever, Celina Ancheta</p>
																	  </div>
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> 
													<a href="#jemimaModal" role="button" data-toggle="modal">
													<span><img src="images/portfolio/jemima.jpg" alt="Portfolio Filter" /></span>
													</a>
                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Bridesmaid </h4> 
													<p><a href="#jemimaModal" role="button" data-toggle="modal">Jemima Davis</a></p>
														<!-- Modal -->
														<div id="jemimaModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Jemima Davis</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/mima1.jpeg" />
															  </p>
															  
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/mima2.jpeg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/mima3.jpeg" />
															  </p>
															  
															  </div>
															  <div class="span6"><p>Considering we were cut from the same fabric, I would say we first met in the baby making workshop, but if you mean literally we met through an ex-boyfriend when you were working at the "Head Game".  That relationship didn't last, but ours has, so I guess that goes to show you there are no accidents.  Before you the only marathons I did were on Netflix, that all changed come 2010, the CIM, a first for both of us, but a last for me.  Very seldom are we fortunate enough to find people who we befriend effortlessly, you meet by chance but you're friends by choice.  Your raw humor, your constant butt grabbing, your contagious laugh, your humble upbringing, your love for cutting up a rug, our hour long philosophical convos,  your constant encouragement through life's ups and downs, and countless other reasons why I can't get enough of (y)our friendship.  And since you and Terrell have been committed to each other for the past 8 years, marriage is the next logical step but from my experience nothing really changes. You will continue to be each other's best friend, you will continue to look forward to date night, you will continue to encourage each other to achieve your goals, you will continue to randomly get on each other's nerves, you will continue to wake up each morning grateful to have found one another.  I'm honored to have been chosen to be a part of your bridal party, a task I will hold true not only today but throughout your relationship, as a unbiased council, as a cheerleader for your love, as a confidant and friend for you both. I look forward with great anticipation when I witness you and Terrell exchange vows of your love, honor and friendship for a lifetime.   </p></div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
												    <div class="filter-thumb"> 
													<a href="#elizebethModal" role="button" data-toggle="modal">
													<span><img src="images/portfolio/elizebeth.jpeg" alt="Portfolio Filter" /></span>
													</a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Bridesmaid </h4> 
													 <p><a href="#elizebethModal" role="button" data-toggle="modal">Elizebeth Ho</a></p>
														<!-- Modal -->
														<div id="elizebethModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Elizebeth Ho</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/liz1.jpeg" />
															  </p>
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/liz2.jpeg" />
															  </p>	 
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/liz3.jpeg" />
															  </p>	 
															  </div>
															  <div class="span6"><p>Laffy Taffy pretty much would describe how my Taffy, bride to be, and I met. She is no doubt the sweetest and man oh man, she and I can laugh for days over anything and everything. It's never a dull moment when we get together, even when we can't get loud we do our infamous horse laugh. We would FaceTime sitting in our toilets just cracking up. No shame straight thugging it out ha! They say it's love at first sight, and I can concur and also add to that- how I knew when we first met at our friends DB's BBQ, we would be the best of friends.The day we met was the day she also voluntarily invited herself to my go away luau. Looking at this curly sue girl all cheesing, I was like "Ohhhh okayyy guess you're invited even though I don't know you lol!" I knew she was special!  While I resided in LA and she was in Roseville, we never allowed the distance to interfere with our friendship. I can honestly say that we can go on months or even years (sad to say) without seeing each other but when we finally do, it feels like where we left off. Taffy is definitely a rare gem that most people can't find to be called their friend. I am beyond blessed to have met her and have her in my life for all these years! I'm truly excited and wish her all the love and happiness as she embarks on her new chapter and journey as being the wife of Timmy. I can't wait to see the love between you two blossom into a big garden of babies,,, little mini TaffyTimmys! I love you two!!! Mahalo &#10084;</p></div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb">
													<a href="#ashleyModal" role="button" data-toggle="modal">
														<span><img src="images/portfolio/ashley.jpg" alt="Portfolio Filter" /></span>
													</a>
                                                    </div>
                                                </div>
                                           <div>
                                                     <h4>Bridesmaid </h4> 
													 <p><a href="#ashleyModal" role="button" data-toggle="modal">Ashley Frkovich</a></p>
														<!-- Modal -->
														<div id="ashleyModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Ashley Frkovich</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/ashley1.jpg" />
															  </p>
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/ashley2.jpg" />
															  </p>	 
															  </div>
															  <div class="span6"><p>My name is Ashley. I have known Taffy since childhood, but our friendship really blossomed our freshman year of high school. She is one of the most loyal and caring people I have ever met. I feel so lucky to have her in my life. Our friendship is so special that no matter how much things in life change, or how far away we may be from each other geographically....when we see each other we always pick up right where we left off :) </p></div>
														  
															
														  </div>
														</div>
														</div>
                                            </li>
											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <a href="#meganModal" role="button" data-toggle="modal"><span><img src="images/portfolio/megan.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Bridesmaid </h4> 
													 <p><a href="#meganModal" role="button" data-toggle="modal">Megan Snatchko</a></p>
														<!-- Modal -->
														<div id="meganModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Megan Snatchko</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/meg0.jpeg" />
															  </p>
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/meg1.jpeg" />
															  </p>	 
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/meg2.jpeg" />
															  </p>	 
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/meg3.jpeg" />
															  </p>	 
															  </div>
															  <div class="span6"><p>I met Taffy through Ashley Frkovich, while we were attending Rocklin High School. It wasn't until I believe our Sophomore year that we became very close and boy did we have some fun! Anytime Taffy was around, it was nothing but laughter, dancing, singing, jokes and just plain fun. Still, to this day that rings true. All of my most favorite and fun memories include Taffy and I know they always will! When I met Taffy, not only did I gain one of the greatest friendships, but also an amazing family! I am so blessed and incredibly lucky to call Taffy one of my best friends. I am so happy for Taffy and Terrell and I can't wait to celebrate with them! Love you guys! </p></div>

														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Bridesmaids">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <a href="#timeshaModal" role="button" data-toggle="modal"><span><img src="images/portfolio/timesha.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Bridesmaid </h4>
													 <p><a href="#timeshaModal" role="button" data-toggle="modal">Timesha Livingston</a></p>
														<!-- Modal -->
														<div id="timeshaModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Timesha Livingston</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/mesha1.jpg" />
															  </p>
															  </div>
															  <div class="span6"><p>How did I meet Taffy? I actually don’t remember the day very well, but it seems more like she’s been with us all along. I do remember that I was shy around her and never really knew what to talk about at first, but slowly conversations about our common interests came to light. She and Terrell would come over to my parents’ house and we would all just talk about random things, especially at holiday dinners. I remember the first time she made us these delicious sushi rolls and I was so amazed and excited because I’ve never seen anyone make them before... I thought you could only get them at restaurants! She inspired me to try and make my own. Congratulations to Taffy and my older brother, Love You Guys!</p></div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											
                                            <li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <a href="#dwayneModal" role="button" data-toggle="modal"><span><img src="images/portfolio/dwayne.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Bestman</h4> 
													 <p><a href="#dwayneModal" role="button" data-toggle="modal">Dwayne Doyley</a></p>
														<!-- Modal -->
														<div id="dwayneModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Dwayne Doyley</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															 <img  class ="memories_img" src ="images/idea_images/memories/dwayne_0.jpg" />
															  </p>
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne_new_orleans.jpg" />
															  </p>	 															 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne0.jpg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne1.jpg" />
															  </p>	 
															    <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne2.jpg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne3.jpg" />
															  </p>	
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne4.jpg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne5.jpg" />
															  </p>	 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne6.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne7.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne8.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne9.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne10.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne12.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne13.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne14.jpg" />
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/dwayne15.jpg" />
															  </p>
															  </div>
															  <div class="span6"><p>I met Terrell over 10 years ago, but I feel like I've known him forever.  We first started running into each other when he worked at Champs Sports. During that time we ended up going to the same college. How we became close was our mutual  interests in activities (too many to mention), as well as our passion for technology.  I was a Computer Science major at the time, he was a Information Technology/Graphic Design major.  We could always bounce ideas off of each other. During college, he lived with us without actually living with us. Our relationship grew after college. We matured and learned a lot about life together and ever since, we've been like brothers. He has been a consistent part of my life through the good and bad times.</p>
																
																<p>Till this day, I can still remember meeting Taffy at my apartment.  She was quick enough to trade verbal jabs with me, I was instantly impressed. Early in the relationship, Taffy and Terrell's relationship was progressing, and Terrell mentioned I should fly out to Cali to meet her family, so I did. Pat took me in like I was her own son. I knew from that moment their relationship was going to last. There was just something different about the way they interacted with each other.  Next thing you know she's living in Boston. Fast forward a few years and they are getting married!  It's crazy how fast life flies by. Together they are a great match and I'm so happy I can be a part of the wedding their wedding day.  Let me be the first one to tell you CONGRATULATIONS!!!  I'm so excited to share your special day and wish you many more wonderful years together. Lots of Love!
																</p></div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"><a href="#marquisModal" role="button" data-toggle="modal"> <span><img src="images/portfolio/marquis.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
													 <p><a href="#marquisModal" role="button" data-toggle="modal">Marquis Harris-Turner</a></p>
														<!-- Modal -->
														<div id="marquisModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Marquis Harris-Turner</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															 <img  class ="memories_img" src ="images/idea_images/memories/quis5.jpg" />
															  </p>
															 <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/quis1.jpg" />
															  </p>	 															 
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/quis2.jpg" />
															  </p>	 
															  </div>
															  <div class="span6"><p>Timothy or Terrell, which ever name you choose to call him&#8212; we go way back. We first met through one of my 104 brothers. Our first encounters was always a, "What's up!", then we would always keep-it-moving.  Our friendship really began to take off when Terrell became a roommate, even though he didn't technically live in our apartment&#8212; he was always there.  Around 2003 to 2004 we made it official and Terrell became a roommate. The spot we had was packed with a bunch of college kids in their last years trying to graduate from University of Massachusetts of Lowell.  From 2004 and on Terrell, me, and our boy Jones got our own spot for a couple years. In those years through our conversations and hanging out with Terrell, he became more of a brother to me and not just a friend. So when the time came he introduced me to Taffy, I knew there was something special about her. Terrell's face lit up every time he spoke about her. He said she was from Cali by way of Hawaii.  When the crew met her, we all knew she would be the one for Tim/Terrell. Which brings us to today, I am glad to see his best friend Taffy, become his soon to be wife. I am looking forward to celebrating this special day with the both of them. </p></div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"><a href="#emmanualModal" role="button" data-toggle="modal"> <span><img src="images/portfolio/emmanual.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
													<p><a href="#emmanualModal" role="button" data-toggle="modal">Emmanual Lamour</a></p>

														<!-- Modal -->
														<div id="emmanualModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Emmanual Lamour</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															
															  <img  class ="memories_img" src ="images/idea_images/memories/manny0.jpg">
															  <br /><br />
															
															  <img  class ="memories_img" src ="images/idea_images/memories/manny1.jpg">
															<br /><br />
															  <img  class ="memories_img" src ="images/idea_images/memories/manny3.jpg">
															  </div>
															  <div class="span6"><p>My name is Emmanuel Lamour; I also go by Manny and sometimes called the Milk Man because I always deliver.  Terrell and I have been friends since our days in college and to keep it short Terrell has become one of my brothers and our lives have been an open book with both of us making contributions as the chapters in our lives have developed.  Terrell is an outgoing, hardworking, and devoted individual that has found a life partner in Taffy that complements him in every which way.</p>
															
															<p>I remember the first time I met Taffy, I was trying to mount one of the older 50" plasma TVs and I asked Terrell to hold one end and Taffy offered to help as well.  After what may have seem like a struggle for Terrell (he will surely tell you differently) Taffy took hold of the entire 100 or so pound TV, although I was impressed I was more concerned for Terrell's safety  just in case he was ever one day to step out of line, lol.</p>
															
															<p>I am happy for you guys and I can attest that this journey that you two are about to take will certainly not disappointment as long as you commit to each other.  I love you both and I am happy to be part of the beginning of the rest your lives.</p>
															</div>

														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <a href="#jackyModal" role="button" data-toggle="modal"><span><img src="images/portfolio/jacky.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
													<p><a href="#jackyModal" role="button" data-toggle="modal">Jacky Ingram</a></p>

														<!-- Modal -->
														<div id="jackyModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Jacky Ingram</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  
															  <img  class ="memories_img" src ="images/idea_images/memories/jacky1.jpg">
															  <br /><br />
															  <img  class ="memories_img" src ="images/idea_images/memories/jacky2.jpg">
															<br /><br />
															  <img  class ="memories_img" src ="images/idea_images/memories/jacky3.jpg">
															  </div>
															  <div class="span6"><p>Hi, I am Jacky, and my life revolves around the following: "Family First".  I have a beautiful and intelligent wife, a strong willed and bright son, and a soon to be born daughter. I view Tim as my tall and quirky brother and Taffy as my thoughtful and hardworking sister. I met Tim over ten years ago in college. We were both in the library studying for our nuclear physics exam...I wish that were the case! Tim and I first met briefly at a party through my childhood friend, and fellow groomsman, Dwayne. Fellowship started with a quick handshake and off we went. As time progressed, we saw each other more frequently at the gym, cookouts, and other functions. Our conversations went from the obligatory "What's up" to substantive and life molding dialogs. I know if I ever need a vent session he will be there with a bucket of golf balls and an open ear. Tim always talked about a girl from California (Taffy); I think back then he described her as "fly". One would always know when Taffy was on the phone, Tim's voice would suddenly deepen and his swagger was turned up. Taffy and I first met, face to face, in 2010 when the "Fellas" took the "Ladies" down to Rhode Island for Valentines weekend. From the onset, Taffy just organically fit into the family. She never passes up the opportunity for an adventure and she is relentless when it comes to achieving her goals. Most importantly, Taffy is a great balance for Tim.</p><p>
Tim and Taffy, I thank you for letting me be a part of your special day. Watch out San Francisco here we come!!!</p></div>

														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"><a href="#denzelModal" role="button" data-toggle="modal"> <span><img src="images/portfolio/denzel.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
													<p><a href="#denzelModal" role="button" data-toggle="modal">Denzel Livingston</a></p>

														<!-- Modal -->
														<div id="denzelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Denzel Livingston</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <img  class ="memories_img" src ="images/idea_images/memories/denzel1.jpg">


															  </div>
															  <div class="span6"><p>Not many people can say their brother inspired them. I mean really... what kind of brother likes to hear that his older brother is awesome. To be honest, I did. Not only did I want to grow up to be like him, I wanted to grow up to be just as successful and hardworking as him. At first I will admit meeting Taffy for the first time was scary. I thought she was going to take my brother from me as corny as that sounds but she brought us even closer. I'm glad to have met her. She added more to this family than I could have imagined. The family that she has been a part of since day one.</p>
																<p>Congratulations Taffy and Terrell! </p>
															</div>

														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <a href="#georgeModal" role="button" data-toggle="modal"><span><img src="images/portfolio/george.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
													<p><a href="#georgeModal" role="button" data-toggle="modal">George Gause</a></p>

														<!-- Modal -->
														<div id="georgeModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">George Gause</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
			
															  <img  class ="memories_img" src ="images/idea_images/memories/george1.jpg">
															  </p>
															  <p> 
															  <img  class ="memories_img" src ="images/idea_images/memories/george2.jpg">
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/george3.jpg">
															  </p>
															  </div>
															  <div class="span6"><p>I met Tim aka Terrell (goof) back in middle school. Even though we went to different middle schools within the same town, we got to know each other through our after school track and field program. During high school, I could almost always be found hanging out at his house before our football and basketball games. After high school we both went on to pursue our college careers. Mine took me to play division one football for University South Carolina Gamecocks and Tim went to University of Massachusetts. We were miles apart and pursuing different dreams, but we managed to keep in touch. Even through my days in the NFL, Tim and I kept a close relationship. Even though we don't get to see each other often, I'm so glad we have remained close. Little did I know that years ago when he told me about the girl (Taffy) he met through AOL (how old school is that?!), that one day I would be booking a trip to California for their wedding. I am thrilled to see him with someone as wonderful as Taffy and look forward to being a part of their big day.  </p></div>

														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> 
													<a href="#giovanniModal" role="button" data-toggle="modal">
													<span><img src="images/portfolio/giovanni.jpg" alt="Portfolio Filter" /></span>
													</a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
													 <p><a href="#giovanniModal" role="button" data-toggle="modal">Giovanni Pagan</a></p>
														<!-- Modal -->
														<div id="giovanniModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Giovanni Pagan</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															<p>
															  <img  class ="memories_img" src ="images/idea_images/memories/gio.jpg" />
															  </p><p>
															  <img  class ="memories_img" src ="images/idea_images/memories/gio3.jpg" />
															  </p><p>
															  <img  class ="memories_img" src ="images/idea_images/memories/gio2.jpg" />
															</p>
															  </div>
															  <div class="span6"><p>I met Tim through mutual friends. Most of our friends shared the same interest and we hung out at the same places. It was only right that we became roommates when we started looking for a place at the same time. We lived together for almost three years. During that time I met Taffy&#8212; actually I was the first friend of Tim's she met. He was pretty excited about meeting her and I thought she was a nice woman. I knew she was special when Tim told me he was moving out, because Taffy was moving from California to Boston to be with him. I was in shock and in disbelief he was dumping me to be with her. LOL... But seriously, till this day&#8212;I always tell Tim, he has an amazing girl and he better not f-that-up. After Taffy and Tim moved in together, we still hung out and I got to know Taffy even better on the group trips we took together. I think they have something amazing going and wish them the best! </p></div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> 
													<a href="#nnamdiModal" role="button" data-toggle="modal">
													<span><img src="images/portfolio/nnamdi.jpg" alt="Portfolio Filter" /></span>
													</a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
													 <p><!-- Button to trigger modal -->
														<a href="#nnamdiModal" role="button" data-toggle="modal">Nnamdi Nwachukwu</a></p>

														<!-- Modal -->
														<div id="nnamdiModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Nnamdi Nwachukwu</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/valance.jpg">
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/valance5.jpg">
															</p>
															<p>
															  <img  class ="memories_img" src ="images/idea_images/memories/valance4.jpg">
															  </p>
															  </div>
															  <div class="span6"><p>What can I say about Mr. Jordan? We met while I was still in high school and we clicked immediately. We seemed to be the only people in suburban Boston who looked and talked differently. I can honestly say that I've known Terrell longer than most of my friends, and he is a loyal, dependable and caring person. A closet geek, with more gadgets than Best Buy, but no matter what the circumstance, always smiling and looking to have a good time.  </p>

																 <p>When I heard about Taffy, I knew something was different, and after I met her, it very was easy to see why. She is a beautiful ball of fun, a part-time comedian, with undeniable intelligence who does not shy away from a battle of wit. Her strengths counter his weaknesses and they are indeed a perfect match. Throughout their relationship, they have both made sacrifices and displayed their commitment to each other. I wish them all that is good in life and a blessed union. </p>
																</div>
														  
															
														  </div>
														</div>
                                                </div>
                                            </li>
											<li data-tags="Groomsmen">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <a href="#lintonModal" role="button" data-toggle="modal"><span><img src="images/portfolio/linton.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Groomsmen</h4> 
														<p><a href="#lintonModal" role="button" data-toggle="modal">Linton Scarlett</a></p>

														<!-- Modal -->
														<div id="lintonModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Linton Scarlett</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/linton1.jpg">
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/linton2.jpg">
															  </p>
															  </div>
															  <div class="span6"><p>Tim, Terrell Jordan and I met over 10 years ago in college at Umass Lowell through a friend. After graduating college we both ended up living around the corner from each other and kept in contact. From then we hung out a lot & got to know each other and really clicked & now are like brothers. Tim was one of the guys that I could call if needing help with something and he'd be there. When Tim told me about Taffy and her moving from the West Coast to the East Coast, I knew it was serious. I met and got to know Taffy and knew that Terrel had a good woman, & told Terrell he better not  mess up, because there aren't a lot of good women left out here...lol. And here we are two great people joining as one!  I'm glad that both of you came across each others path and now taking it to the next chapter. I am honored to be apart of this special time in your lives. We're going going, back, back to Cali Cali!!</p>

																<p>Blessings to you guys!</p>
															</div>
															</div>

														  </div>
														</div>
                                            
                                            </li>
											<li data-tags="Officiant">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"><a href="#sueModal" role="button" data-toggle="modal"> <span><img src="images/portfolio/suejen.jpg" alt="Portfolio Filter" /></span></a>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Officiant </h4> 
														<p><a href="#sueModal" role="button" data-toggle="modal">Suejen Santiago</a></p>

														<!-- Modal -->
														<div id="sueModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														  <div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
															<h3 id="myModalLabel">Suejen Santiago</h3>
														  </div>
														  <div class="modal-body">
															  <div class="span6">
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/suejen1.jpg">
															  </p>
															  <p>
															  <img  class ="memories_img" src ="images/idea_images/memories/suejen2.jpg">
															</p>
															  </div>
															  <div class="span6"><p>Waiting on paragraph....</p></div>

														  </div>
														</div>
                                                </div>
                                            </li>
											
											<li data-tags="Ushers">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <span><img src="images/portfolio/malik.jpg" alt="Portfolio Filter" /></span>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Ushers</h4> 
													 <p>Malik Humes</p>
                                                </div>
                                            </li>
											
											<li data-tags="Ushers">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <span><img src="images/portfolio/tyrek.jpg" alt="Portfolio Filter" /></span>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Ushers</h4> 
													 <p>Tyrek Humes</p>
                                
                                                </div>
                                            </li>
											<li data-tags="Flower Girl">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <span><img src="images/portfolio/keke.jpg" alt="Portfolio Filter" /></span>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Flower Girl</h4> 
													 <p>Ke'era Chalk</p>
                                                </div>
                                            </li>

											<li data-tags="Ring Bearer">
                                                <div class="filter-thumb-container">
                                                    <div class="filter-thumb"> <span><img src="images/portfolio/jaylen.jpg" alt="Portfolio Filter" /></span>

                                                    </div>
                                                </div>
                                                <div>
                                                     <h4>Ring Bearer</h4> 
													 <p>Jaylen Basque</p>
                                                </div>
                                            </li>

                                        </ul>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--#third-->
                <div id="rsvp">
            </div>
                <!--#LargeSlider-->
                <div id="fourth" class="Parallax">
                    <div class="ParallaxText">
                        	<h2>RSVP - Coming soon...<br /> Wedding date - June 5th, 2015</h2>

                        <div class="clearfix"></div>
                      <!-- <blockquote>							
						<form class="form-horizontal span6">
							  <div class="control-group">
								<div class="controls">	
								  <label class="control-label" for="inputEmail">First Name</label>
								  <input type="text" id="inputEmail" placeholder="Email">
								</div>
							  </div>
							  <div class="control-group">
								<div class="controls">
								  <label class="control-label" for="inputPassword">Last Name</label>
								  <input type="password" id="inputPassword" placeholder="Password">
								</div>
							  </div>
							  <div class="control-group">
								<div class="controls">
								  <button type="submit" class="btn">Sign in</button>
								</div>
							  </div>
							</form></blockquote>--> 
                        <div class="clearfix"></div><br /><br /><br />
                        <p>- RSVP sign up will open when  invitations go out, please check back. </p>
                    </div>
                </div>
                <!--#fourth-->
                <div id="details">
                    <div class="container-fluid clearfix Contact">
                        <div class="container clearfix TitleSection">
                            
					<h1>Wedding Details......</h1>

                        </div>
                        <div class="container clearfix">
                            <div class="container">
								
								<div class="span12">
									<h2>June 5th, 2015</h2>
									<p>
									<strong>Ceremony & Reception at:</strong><br />
									Golden Gate Club<br />
									135 Fisher Loop <br />
									San Francisco, CA 94129<br />
									*Start Time: 5:00 PM <br />
									<img  style="float left;"  src ="images/golden-gate-club.jpg">
									<img style="float left; padding-left:5px;"  src ="images/golden_gate.jpeg">
									 </p>
									<hr >
								</div>
							

							
								<div class="span8">
									<h2>Hotel Information</h2>
									<p>For your convenience we have blocked off two hotels, Sheraton Hotel and Holiday Inn. The hotels are relatively close to each other. Taffy and Tim will be staying at the Holiday Inn. 
										</p>
								</div>
							
                                <div class="span5">
                                    <div class="well">
                                        <p>Sheraton Fisherman's Wharf Hotel</p>
                                        <hr>
                                        <div class="contact-info">
                                            <ul>
                                                <li> <i class="icon-book" style="margin-right:10px;  float: left; height: 20px;"></i><span style="display:block; "><a href="https://www.starwoodmeeting.com/StarGroupsWeb/res?id=1409082225&key=222A85A4" target="_blank">Online CLICK HERE</a> for Basque - Jordan Wedding Room Block <br />
												June  3 - 7, 2015.</span>
												</li>
                                                <li> <i class="icon-globe" style="margin-right:10px"></i>Address: 2500 Mason St 94133 San Francisco</li>
                                                <li style="margin-bottom:14px;"> <i class="icon-bullhorn" style="margin-right:10px;"></i>Hotel Reservations: 888-627-7024
												
												<p> To receive the special group rates, the attendees should request the name of the group: “Basque/Jordan Wedding Block” </p>
												
												<p>Group rate: $199.00 per room, per night, plus tax, single/double occupancy. Plus $20 per additional occupant with existing bedding.</p>
												
												<p>All reservations should be made by <strong>Sunday, May 4, 2015</strong> (Subject to Availability).  After this date, reservation requests will be honored based on availability at prevailing rates. </p>
												
												</li>
                                            </ul>
                                        </div>
										
									<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script><div style="overflow:hidden;height:500px;width:100%;"><div id="gmap_canvas" style="height:500px;width:100%;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.map-embed.net" id="get-map-data">www.map-embed.net</a></div><script type="text/javascript"> function init_map(){var myOptions = {zoom:17,center:new google.maps.LatLng(37.806972,-122.41333470000001),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(37.806972, -122.41333470000001)});infowindow = new google.maps.InfoWindow({content:"<b>Sheraton Fisherman's Wharf Hotel</b><br/>2500 Mason St<br/>94133 San Francisco" });google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>

                                    </div>
                                </div>
								
                                <div class="span5">
                                    <div class="well">
                                        <p>Holiday Inn - Fisherman's Wharf</p>
                                        <hr>
                                        <div class="contact-info">
                                            <ul>
												<li> <i class="icon-book" style="margin-right:10px;  float: left; height: 20px;"></i><span style="display:block; "><a href="https://www.ihg.com/holidayinn/hotels/us/en/san-francisco/sfofw/hoteldetail" target="_blank">Online CLICK HERE</a> for Basque - Jordan Wedding Room Block <br />
												June  3 - 7, 2015. <strong> Enter the online booking code:</strong> <strong style="color:red">BJW</strong>.</span>
												</li>
											
                                                <li> <i class="icon-globe" style="margin-right:10px"></i>Address: 1300 Columbus Ave, San Francisco, CA 94133
												</li>
                                                <li style="margin-bottom:14px;"> <i class="icon-bullhorn" style="margin-right:10px;"></i>Hotel Reservations: 800-942-7348<br />
												<p> To receive the special group rates, the attendees should request the name of the group: “Basque/Jordan Wedding Block” </p>
												
												<p>Group rate: $209.00 per room, per night, plus tax, single/double occupancy. Plus $20 per additional occupant with existing bedding.</p>
												
												<p>All reservations should be made by <strong>Sunday, May 3, 2015</strong>.  After this date, reservation requests will be honored based on availability at prevailing rates. </p>
												</li>
                                            </ul>
                                        </div>
										
									<div style="overflow:hidden;height:500px;width:100%;"><div id="gmap_canvas2" style="height:500px;width:100%;"></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style><a class="google-map-code" href="http://www.map-embed.net" id="get-map-data">www.map-embed.net</a></div>
									
								<script type="text/javascript"> function init_map() {
									 var myOptions2 = {
										 zoom: 17,
										 center: new google.maps.LatLng(37.8065976, -122.41817730000002),
										 mapTypeId: google.maps.MapTypeId.ROADMAP
									 };

									 map2 = new google.maps.Map(document.getElementById("gmap_canvas2"), myOptions2);
									 marker2 = new google.maps.Marker({
										 map: map2,
										 position: new google.maps.LatLng(37.8065976, -122.41817730000002)
									 });
									 infowindow2 = new google.maps.InfoWindow({
										 content: "<b>Holiday Inn</b><br/>1300 Columbus Ave<br/>94133 San Francisco"
									 });
									 google.maps.event.addListener(marker, "click", function() {
										 infowindow2.open(map2, marker2);
									 });
									 infowindow2.open(map2, marker2);
								 }
								 google.maps.event.addDomListener(window, 'load', init_map);
								</script>
									
									
                                    </div>
                                </div>
                            </div>
							
							<hr>
	
							<h2>Golden Gate Club</h2>
							<img src="images/map_layout.jpg" width="100%">
							
                        </div>
                    </div>
                </div>

                <!--#fifth-->
                <div class="container-fluid Footer">
                    <div class="container">
                        <footer>
                           
                            <p>Taffy and Tim's long awaited wedding  day - June 5th, 2015.  All rights reserved.
                            </p>
                        </footer>
                    </div>
                </div>
            </div>
            <!-- /Row -->

	<div class="musicPlayer">
	<audio id="track1" controls autoplay>
	  <source src="audio/Loving_You_Is_Killing_Me.mp3" type="audio/mp3">
	Your browser does not support the audio element. Best viewed in Firefox, IE9 & above, and Chrome 
	</audio>
	</div>	
			
        <!-- /container -->
        <div id="toTop">To Top</div>
        <!-- Le javascript==================================================-->
    <!-- Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.8.2.min.js"></script>
        <script src="js/lean-slider.min.js"></script><!-- About Slider-->
        <script src="js/my_script.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.easing.1.3.min.js"></script><!-- parallax-->
        <script src="js/modernizr-2.6.1.min.js"></script><!--blur slidShow -->
        <script src="js/jquery.quicksand.min.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/jquery.mobile.customized.min.js"></script><!-- camera Slider-->
        <script src="js/camera.min.js"></script><!-- camera Slider-->
        <script src="js/jquery.parallax-1.1.3.min.js"></script><!--  parallax-->
        <script src="js/jquery.localscroll-1.2.7-min.js"></script><!--  parallax-->
        <script>
			myMid=document.getElementById("track1");
			myMid.preload="auto";
		
/*             jQuery(function () {

                jQuery('#camera_wrap_3').camera({
                    height: '65%',
                    pagination: false,
                    thumbnails: true,
                    imagePath: '../images/'
                });

            }); */
        </script>
    </body>

</html>